<?php
include_once __DIR__ .'/Server.php';
include_once __DIR__ .'/Engine.php';

class Browser {
    private $engine;
        
    public function __construct() 
    {
        $this->engine = new Engine();
    }

    public function GetPage($url)
    {
        $versions =  $this->engine->getSupportedHtmlVersions();
        
        $server = new Server();
        
        foreach ($versions as $version) 
            {
            if ($server->isHtmlVersionSupported($version)) 
                {
                $server->setHtmlVersionToUse($version);
                $response = $server->getResponse();
                
                return $this->engine->parse($response);
                }
            }
        
    }
}

?>
